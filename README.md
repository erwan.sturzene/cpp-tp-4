# TP 5 - Optimisation

### 1) Memory leak

[Valgrind before any changes](./readme-docs/val_before.xml)

[Valgrind after optimization and corrections](./readme-docs/val_after.xml)

### 2) Optimisations
Malheureusement, google-gprof ne fonctionne pas... Mais en utilisant le profiler par défaut de CLion, j'ai pu obtenir les résulats suivants:

```
=============== NO OPTIMISATION (Branch WithoutOpti) ===============
Finished calculation (1000) in avg.	49ms, total	245ms
Finished calculation (30000) in avg.	25435ms, total	127175ms
Finished calculation (30000) in avg.	30109ms, total	150547ms
Finished calculation (30000) in avg.	58337ms, total	291689ms

================= PRAGMA DISTANCE (Branch Opti1)  ==================
Finished calculation (1000) in avg.	22ms, total	111ms
Finished calculation (30000) in avg.	2300ms, total	11501ms
Finished calculation (30000) in avg.	7718ms, total	38590ms
Finished calculation (30000) in avg.	20063ms, total	100315ms

================= PRAGMA RANGE TREE (Branch Opti2) ==================
Finished calculation (1000) in avg.	13ms, total	69ms
Finished calculation (30000) in avg.	943ms, total	4716ms
Finished calculation (30000) in avg.	599ms, total	2997ms
Finished calculation (30000) in avg.	839ms, total	4196ms

================= PRAGMA EVERYWHERE (Branch Opti3) ==================
Finished calculation (1000) in avg.	8ms, total	41ms
Finished calculation (30000) in avg.	410ms, total	2051ms
Finished calculation (30000) in avg.	384ms, total	1922ms
Finished calculation (30000) in avg.	628ms, total	3142ms
```
#### Evolution of the optimization
![](./readme-docs/EvolutionOfTime.png)

#### Evolution of the process time
![](./readme-docs/EvolutionOfOptimization.png)
