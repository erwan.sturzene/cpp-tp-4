//
// Created by Beat Wolf on 30.11.2021.
//

#include <iostream>
#include "RangeTree.h"

RangeTree::RangeTree(std::vector<Point *> points) {
    //Sort all points by X
    std::sort(points.begin(), points.end(), [](Point *a, Point *b) {
        return a->getX() < b->getX();
    });

    //Build using all points (sorted in X)
    this->xTree = build2DRangeTree(points, 0, points.size());
}

/**
 * Builds a 2D range tree from a list of points
 * @param points List of points
 * @param left   Left index
 * @param right  Right index
 * @return     Root node of the tree
 */

BinaryTree<BinaryTree<Point *, double>, Point *> RangeTree::build2DRangeTree(std::vector<Point *> &points,
                                                                             size_t left,
                                                                             size_t right) const {
    BinaryTree<BinaryTree<Point *, double>, Point *> tx;

    if (left >= right) {
        return tx;
    }


    std::vector<Point *> yPoints{points.begin() + left, points.begin() + right};
    BinaryTree<Point *, double> ty(yPoints,
                                   [](const Point *a) { return a->getY(); },
                                   true);

    //Construct xTree
    size_t mid = (left + right) / 2;
    tx.setRoot(points[mid], ty);

    // Construct left and right subtrees
    tx.getRoot()->left = build2DRangeTree(points, left, mid).getRootCopy();
    tx.getRoot()->right = build2DRangeTree(points, mid + 1, right).getRootCopy();

    return tx;
}

std::vector<Point *> RangeTree::search(const Point &start, const Point &end) const {
    std::vector<Point *> result;
    search(result, xTree.getRoot(),
           start.getX(), end.getX(),
           std::numeric_limits<int>::min(), std::numeric_limits<int>::max(),
           start.getY(), end.getY());

    return result;
}

bool RangeTree::isEmpty() const {
    return xTree.isEmpty();
}

size_t RangeTree::size() const {
    return xTree.size();
}

/**
 * Searches for all points in the range [startX, endX] and [startY, endY] and adds them to the result vector.
 * @param result   The result vector.
 * @param node   The current node.
 * @param xFrom  The start x value.
 * @param xTo  The end x value.
 * @param xMin  The minimum x value.
 * @param xMax  The maximum x value.
 * @param yFrom  The start y value.
 * @param yTo  The end y value.
 */
void RangeTree::search(std::vector<Point *> &result,
                       BinaryTreeNode<BinaryTree<Point *, double>, Point *> *node,
                       double xFrom, double xTo,
                       double xMin, double xMax,
                       double yFrom, double yTo) const {

    // If the node is empty, return
    if (node == nullptr) {
        return;
    }

    if (node->key->getX() > xTo) {
        // If the current node is greater than the search range, we can skip it and search the left subtree
        search(result, node->left, xFrom, xTo, xMin, node->key->getX(), yFrom, yTo);
        return;

    } else if (node->key->getX() < xFrom) {
        // If the current node is smaller than the search range, we can skip it and search the right subtree
        search(result, node->right, xFrom, xTo, node->key->getX(), xMax, yFrom, yTo);
        return;
    }
    if (xMin >= xFrom && xMax <= xTo) {
        // List and store all points in this node corresponding to the search area
        node->value.getRoot()->inRange(result, yFrom, yTo);
        return;
    }

    // Check if the current node is in the search area
    if (node->key->getY() >= yFrom && node->key->getY() <= yTo) {
        result.push_back(node->key);
    }

    //Search left and right subtrees
    search(result, node->left, xFrom, xTo, xMin, node->key->getX(), yFrom, yTo);
    search(result, node->right, xFrom, xTo, node->key->getX(), xMax, yFrom, yTo);
}
