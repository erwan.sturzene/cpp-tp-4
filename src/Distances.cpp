//
// Created by "Beat Wolf" on 08.02.2022.
//

#include "base/SimpleDistance.h"
#include "IntervalTree.h"
#include "RangeTree.h"
#include <cmath>

void normalize(std::vector<double> &values){
    //Calculate mean
    double totalDistance = 0;
    #pragma omp parallel for reduction(+:totalDistance)
    for(size_t i = 0; i < values.size(); i++){
        totalDistance += values.at(i);
    }
    double mean = totalDistance /= values.size();

    //Calculate SD
    double totalSD = 0;
    #pragma omp parallel for reduction(+:totalSD)
    for(size_t i = 0; i < values.size(); i++){
        totalSD += std::pow(values.at(i) - mean, 2);
    }
    double sd = std::sqrt(totalSD / values.size());

    //Normalize
    #pragma omp parallel for
    for(size_t i = 0; i < values.size(); i++){
        values.at(i) = (values.at(i) - mean) / sd;
    }
}

/**
 * Calculate the average distance for all points with their neigbhours in a certain range
 * @param points
 * @param min
 * @param max
 * @param region_size
 * @return
 */
std::vector<double> calculateDistances(std::vector<Point *> points, double min, double max, double region_size){
    RangeTree tree(points);

    std::vector<double> distances(points.size());

    int index = 0;
    #pragma omp parallel for
    for(const Point * point : points){
        double width = (max - min) * region_size / 2;

        Point start(*point);
        Point end(*point);
        Point offset(width, width);

        start -= offset;
        end += offset;

        std::vector<Point *> neighbours = tree.search(start, end);

        double totalDistance = 0;
        #pragma omp parallel for reduction(+:totalDistance)
        for(Point *neighbour : neighbours){
            totalDistance += neighbour->distance(*point);
        }

        #pragma omp critical
        distances[index++] = totalDistance / neighbours.size();
    }

    return distances;
}

std::vector<double> filter(std::vector<double> distances){
    std::vector<Interval<double, double>> intervals = createIntervals();

    IntervalTree<double> tree(intervals);

    std::vector<double> distancesFiltered;

    #pragma omp parallel for
    for(double val : distances){
        std::vector<Interval<double, double>> targets = tree.containing(val);
        if(targets.size() > 0){
            #pragma omp parallel for reduction(*:val)
            for(Interval<double, double> v : targets){
                val *= v.getValue();
            }
            #pragma omp critical
            distancesFiltered.push_back(val);
        }
    }

    return distancesFiltered;
}

double optimizedAverageDistanceToNeighbours(std::vector<Point *> points, double min, double max, double region_size){
    std::vector<double> distances = calculateDistances(points, min, max, region_size);

    normalize(distances);

    distances = filter(distances);

    //SUM
    double sum = 0;

    #pragma omp parallel for reduction(+:sum)
    for(size_t i = 0; i < distances.size(); i++){
        sum += distances[i];
    }

    return sum;
}