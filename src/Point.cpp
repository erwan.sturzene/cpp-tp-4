// Copyright 2022 Haute école d'ingénierie et d'architecture de Fribourg
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/****************************************************************************
 * @file Point.cpp
 * @author Erwan Sturzenegger <erwan.sturzene@edu.hefr.ch>
 *
 * @brief Implementation of the Point class
 *
 * @date 05.04.22
* @version 0.1.0
 ***************************************************************************/
#include "Point.h"

Point::Point(const Point &point) {
    this->x = point.x;
    this->y = point.y;
    this->name = point.name;
}

Point::Point(double x, double y, std::string name) :
        x(x), y(y), name(name) {
}

double Point::getX() const {
    return this->x;
}

double Point::getY() const {
    return this->y;
}

std::string Point::getName() {
    return this->name;
}

void Point::setX(double x) {
    this->x = x;
}

void Point::setY(double y) {
    this->y = y;
}

void Point::setName(std::string name) {
    this->name = name;
}

double Point::distance(const Point &point) const {
    return sqrt(pow(this->x - point.x, 2) + pow(this->y - point.y, 2));
}

Point &Point::operator+=(const Point &other) {
    this->x += other.x;
    this->y += other.y;
    return *this;
}

Point &Point::operator-=(const Point &other) {
    this->x -= other.x;
    this->y -= other.y;
    return *this;
}

Point &Point::operator*=(const double other) {
    this->x *= other;
    this->y *= other;
    return *this;
}

Point &Point::operator/=(const double other) {
    if(other == 0) {
        throw std::invalid_argument("Division by zero");
    }
    this->x /= other;
    this->y /= other;
    return *this;
}

Point Point::operator+(const Point &other) {
    Point result(*this);
    result += other;
    return result;
}

Point Point::operator-(const Point &other) {
    Point result(*this);
    result -= other;
    return result;
}

Point Point::operator*(const double other) {
    Point result(*this);
    result *= other;
    return result;
}

Point Point::operator/(const double other) {
    if (other == 0) {
        throw std::invalid_argument("Division by zero");
    }
    Point result(*this);
    result /= other;
    return result;
}


